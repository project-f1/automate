package annotations;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.*;

@SuppressWarnings("unused")
public class LoginFunc {

	@Test
	public void loginViaEmail() {
		System.out.println("Login using EmailID ");
		// Assert.assertEquals("Pras", "Pras");
		// Assert.assertEquals(1, 2);
		// Assert.assertTrue(3>1);
		// if any assertion fails, it ll throw exception and moves to next test, if we
		// still want to continue with the same test, then use try catch block
		try {
			Assert.assertEquals("Pras", "Pr1as");
		}catch(Throwable t) {
			System.out.println(t);
		}
		System.out.println("Printed after assertion failure");
	}

	@Test
	public void loginViaTwitter() {
		System.out.println("Login using Twitter ");
		// throw new SkipException("Twitter login is currently disabled");
	}

	@Test(priority=2)
	public void loginViaFacebook() {
		System.out.println("Login using Facebook ");
	}
	@Test
	public void loginViaQuora() {
		System.out.println("login using Quora");
		// throw new SkipException("Twitter login is currently disabled");
	}
	@Test(priority=1)
	public void loginViaSlack() {
		System.out.println("Login using Slack");
		// throw new SkipException("Twitter login is currently disabled");
	}
	@Test
	public void loginViaPintrest() {
		System.out.println("Login using Pintrest");
		// throw new SkipException("Twitter login is currently disabled");
	}
}
