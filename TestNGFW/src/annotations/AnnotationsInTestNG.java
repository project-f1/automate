package annotations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

public class AnnotationsInTestNG {
	private static WebDriver driver;

	@BeforeClass
	public void prepWork() {
		System.out.println("Initializing selenium");
		System.setProperty("webdriver.gecko.driver",
				"C://Users//Administrator//Desktop//SeleniumFile//geckodriver-v0.29.1-win64//geckodriver.exe");

	}

	@BeforeMethod
	public void preReq() {
		System.out.println("Opening Browser - Before Method");
		driver = new FirefoxDriver();
		driver.get("https://www.selenium.dev");
	}

	@Test
	public void test1() {
		System.out.println("From test1 method");
		System.out.println(driver.getTitle());
	}

	@Test
	public void test2() {
		System.out.println("From test2 method");
		System.out.println(driver.getCurrentUrl());
	}

	@AfterMethod
	public void tearDown() {
		System.out.println("Closing Browser");
		driver.quit();
	}

	@AfterClass
	public void destorySel() {
		System.out.println("Destroy test");
	}

}
