package annotations;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AnnotationsInTestNG2 {
	private static WebDriver driver;

	@BeforeClass
	public void prepWork() {
		System.out.println("Initializing selenium");
		System.setProperty("webdriver.chrome.driver",
				"C://Users//Administrator//Desktop//SeleniumFile//chromedriver_win32//chromedriver.exe");

	}

	@BeforeMethod
	public void preReq() {
		System.out.println("Opening Browser - Before Method");
		driver = new ChromeDriver();
		driver.get("https://www.facebook.com");
	}

	@Test
	public void test1() {
		System.out.println("From test1 method");
		System.out.println(driver.getTitle());
	}

	@Test
	public void test2() {
		System.out.println("From test2 method");
		System.out.println(driver.getCurrentUrl());
	}

	@AfterMethod
	public void tearDown() {
		System.out.println("Closing Browser");
		driver.quit();
	}

	@AfterClass
	public void destorySel() {
		System.out.println("Destroy test");
	}
}
