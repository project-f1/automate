package annotations;

import org.testng.annotations.*;

public class UsingDataProvider {
	@Test(dataProvider = "testData")
	public void loginFunc(String name, String email, String empID) {
	System.out.println(name);
	System.out.println(email);
	System.out.println(empID);
}
	@DataProvider
	public Object[][] testData(){
		Object[][] obj = new Object[3][3];
		obj[0][0] = "Prasanna";
		obj[0][1] = "abc@df.com";
		obj[0][2] = "123654";
		
		obj[1][0] = "Rahul";
		obj[1][1] = "xzc@df.com";
		obj[1][2] = "44312";
		
		obj[2][0] = "Sagar";
		obj[2][1] = "btre@df.com";
		obj[2][2] = "87644";
		return obj;
	}
}
