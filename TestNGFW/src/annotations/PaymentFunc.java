package annotations;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PaymentFunc {

//	@Test(priority=3)
//	public void payViaUPI() {
//		System.out.println("payViaUPI");
//	}
//	@Test(priority=1)
//	public void payViaCC() {
//		System.out.println("payViaCC");
//	}
//	@Test(priority=2)
//	public void payViaNetBanking() {
//		System.out.println("payViaNetBanking");
//	}
	@Test
	public void login() {
		System.out.println("Login Application");
		Assert.assertEquals("Str1", "Str2");
	}
	@Test(dependsOnMethods= {"login"})
	public void navigate() {
		System.out.println("navigating the application");
	}
	@Test(dependsOnMethods= {"login","navigate"})
	public void logout() {
		System.out.println("logout application");
	}
}