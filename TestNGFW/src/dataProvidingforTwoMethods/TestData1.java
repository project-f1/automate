package dataProvidingforTwoMethods;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;

public class TestData1 {
	Object[][] obj;
	@DataProvider(name="data")
	public Object[][] testData(Method m){
		
		if(m.getName().equals("RegisterUser")) {
			obj = new Object[2][3];
			obj[0][0]="pras";
			obj[0][1]="abc";
			obj[0][2]="pras.abc@gmail.com";
			
			obj[1][0]="hgf";
			obj[1][1]="asdf";
			obj[1][2]="hgf.asdf@gmail.com";
			
		}
		if(m.getName().equals("logIN")) {
			obj = new Object[2][3];
			obj[0][0]="pras";
			obj[0][1]="abc";
			obj[0][2]="pras.abc@gmail.com";
			
			obj[1][0]="hgf";
			obj[1][1]="asdf";
			obj[1][2]="hgf.asdf@gmail.com";
		
	}
		return obj;		
}
}