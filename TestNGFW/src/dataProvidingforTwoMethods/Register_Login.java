package dataProvidingforTwoMethods;

import org.testng.annotations.*;

public class Register_Login {
	@Test(dataProviderClass = TestData1.class, dataProvider = "data")
	public void RegisterUser(String firstName, String lastName, String emailID) {
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(emailID);
	}

	@Test(dataProviderClass = TestData1.class, dataProvider = "data")
	public void logIN(String userName, String emailID, String pwd) {
		System.out.println(userName);
		System.out.println(emailID);
		System.out.println(pwd);
	}
}
