package dataProviding;

import org.testng.annotations.*;

public class LoginFunct1 {
	@Test(dataProviderClass = TestDataFile.class, dataProvider = "data")
	public void loginTester(String username, String email, String password) {
		System.out.println(username);
		System.out.println(email);
		System.out.println(password);
	}
}