package dataProviding;

import org.testng.annotations.DataProvider;

public class TestDataFile {

	@DataProvider(name="data")
	public Object[][] testData(){
		Object[][] ob = new Object[3][3];
		ob[0][0] = "horse";
		ob[0][1] = "horse@yahoo.com";
		ob[0][2] = "123horse";
		
		ob[1][0] = "cat";
		ob[1][1] = "cat@yahoo.com";
		ob[1][2] = "123cat";
		
		ob[2][0] = "dog";
		ob[2][1] = "dog@yahoo.com";
		ob[2][2] = "123dog";
		
		return ob;
	}
}
