package testNGgrouping;

import org.testng.annotations.*;

public class Demo {
	
	@Test(groups = {"performance"})
	public void demoTest1(){
		System.out.println("performance Feature1");
	}
	@Test(groups = {"regression"})
	public void demoTest2(){
		System.out.println("Regression Feature2");
	}
	@Test(groups = {"regression"})
	public void demoTest3(){
		System.out.println("Regression Feature3");
	}
	@Test(groups = {"sanity"})
	public void demoTest4(){
		System.out.println("sanity Feature4");
	}
}
