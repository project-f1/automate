package selenium_basics;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WinPopUp {
	private static WebDriver driver;
	private static Set<String> winHandles;
	private static Iterator<String> itr;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		co.addArguments("--no-sandbox");
		co.addArguments("--disable-notification");
		driver = new ChromeDriver(co);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.seleniumeasy.com/test/window-popup-modal-demo.html");
		driver.findElement(By.xpath("//a[text() = '  Like us On Facebook ']")).click();
		String prntID = driver.getWindowHandle();
		System.out.println("Parent window id is: " + prntID);
		winHandles = driver.getWindowHandles();
		System.out.println(winHandles);
		itr = winHandles.iterator();
		while (itr.hasNext()) {
			String chWin = itr.next();
			if (!prntID.equals(chWin)) {
				driver.switchTo().window(chWin);
				System.out.println("Child window Title is: " + driver.getTitle());
			}
		}
		driver.switchTo().window(prntID);
		System.out.println("Parent window Title is: " + driver.getTitle());
		driver.findElement(By.xpath("//a[contains(text(),'Follow On Twitter')]")).click();
		winHandles = driver.getWindowHandles();
		itr = winHandles.iterator();
		while (itr.hasNext()) {
			String chWin2 = itr.next();
			if (!prntID.equals(chWin2)) {
				driver.switchTo().window(chWin2);
				System.out.println("Second child window title is: " + driver.getTitle());
			}
		}
		driver.quit();

	}
}
