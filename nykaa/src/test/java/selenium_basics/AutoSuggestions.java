package selenium_basics;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutoSuggestions {
	private static WebDriver driver;
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		driver.manage().window().maximize();
		// sending the search text
		String searchTxt = "Selenium";
		driver.findElement(By.xpath("//input[@type='text']")).sendKeys(searchTxt);
		List<WebElement> autoSuggestion = driver
				.findElements(By.xpath("//ul[@role='listbox']//li/descendant::div[@class='wM6W7d']"));
		// printing the total number of suggestions
		System.out.println("total suggestion displayed are: " + autoSuggestion.size());
		
		System.out.println("***********auto suggestion displayed are as follows :***************");
		//printing the auto suggestions
		for (WebElement ele : autoSuggestion) {
			System.out.println(ele.getText());
		}
		// selecting the required option of our wish and clicking on it
		System.out.println("*****************Selecting textbook option**********************");
		for (WebElement ele1 : autoSuggestion)
		if(ele1.getText().contains("seleniumhq")) {
			ele1.click();
			break;
		}
		// printing the page title after selecting required option
		System.out.println("Title of the page after searching is - " + driver.getTitle());
		driver.close();
	}
}