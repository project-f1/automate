package selenium_basics;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchingGoogle {
private static WebDriver driver;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.saucedemo.com/");
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		driver.findElement(By.id("user-name")).sendKeys("abc");
		driver.findElement(By.id("password")).sendKeys("def");
		driver.findElement(By.id("login-button")).click();
		WebElement errMsg = driver.findElement(By.cssSelector("div.error-message-container"));
		System.out.println(errMsg.getText());
		driver.close();	
	}
}
