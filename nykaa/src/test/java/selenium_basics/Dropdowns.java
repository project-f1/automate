package selenium_basics;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class Dropdowns {

	public static void main(String[] args) {
		try {
			System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.addArguments("--start-maximized");
			chromeOptions.addArguments("--no-sandbox");
			chromeOptions.addArguments("--disable-notifications");
			// chromeOptions.addArguments("--lang=en-GB"); // #remove this line if you want
			// to set the browser locale to
			// the AWS region you launched this instance

			WebDriver driver = new ChromeDriver(chromeOptions);

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); // general timeout for waiting on dynamic
																				// web elements

			System.out.println("Started Browser and Driver");

			// navigate to chercher.tech/practice
			driver.get("https://chercher.tech/practice/practice-dropdowns-selenium-webdriver");
			System.out.println("Navigated to chercher.tech/");
			Thread.sleep(2000);
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("./src/test/resources/screenshot" + System.currentTimeMillis() + ".png" ));
			//locating the drop down and printing all the available options
			WebElement dropDown = driver.findElement(By.cssSelector(".col-lg-3"));
			Select dpd = new Select(dropDown);
			//print the size of the drop down
			System.out.println("Total number of option present are: " + dpd.getOptions().size());
			// print all the options present in the drop down
			List<WebElement> allOptions = dpd.getOptions();
			for(WebElement ele : allOptions) {
				System.out.println(ele.getText());
			}
			// Selecting a particular value in the dropdown
			//dropDown.click();
			dpd.selectByVisibleText("Iphone");
			Thread.sleep(3500);
			dpd.selectByValue("Microsoft");
			Thread.sleep(3500);
			driver.close();
			
		} catch (Exception ex) {  
			System.out.println(ex.getMessage());
		}
	}
}
