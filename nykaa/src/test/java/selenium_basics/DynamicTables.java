package selenium_basics;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DynamicTables {
	private static WebDriver driver;

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://uitestingplayground.com/dynamictable");
		driver.manage().window().maximize();
		String chromeCPU = driver
				.findElement(By.xpath("//span[text()='Chrome']/following-sibling::span[contains(text(),'%')]"))
				.getText();
		String actualCPU = driver.findElement(By.xpath("//p[contains(text(),'Chrome CPU')]")).getText();
		String[] trimmedCPU = actualCPU.split(":");
		String percentCPU = trimmedCPU[1].trim();
		if (percentCPU.equals(chromeCPU)) {
			System.out.println("CPU percentage matches - Test Passed");
		} else {
			System.out.println("CPU percentage do not match - Test Failed");
		}
		driver.close();
	}

}
