package selenium_basics;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FooterLinks {
	private static WebDriver driver;

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		co.addArguments("--no-sandbox");
		co.addArguments("--disable-notification");
		driver = new ChromeDriver(co);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// WebDriverWait wait = new WebDriverWait(driver, 10000);
		driver.get("https://www.bata.in/");
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		WebElement footerDriver = driver.findElement(By.cssSelector(".cc-footer-main .cc-container-small"));
		WebElement columnDriver = footerDriver.findElement(By.cssSelector(".col-sm-9"));
		List<WebElement> footerLinks = columnDriver
				.findElements(By.xpath("//div[@class='col-12 col-sm-4'] //span/following-sibling::ul/li/a"));
		System.out.println("Total links present in the footer section are: " + footerLinks.size());

		// ********Printing all the links present in the footer section********
		System.out.println("********Printing all the links present in the footer section********");
		for (WebElement ele : footerLinks) {
			System.out.println(ele.getText());
		}

		// ********Printing all the links present in the 1st column of the footer******
		System.out.println("********Printing all the links present in the 1st column of the footer******");
		List<WebElement> firstColLinks = columnDriver.findElements(By.xpath(
				"//div[@class='col-12 col-sm-4'] //span[contains(text(),'Help You')]/following-sibling::ul/li/a"));
		System.out.println("Total links present in the 1st column of footer section are: " + firstColLinks.size());
		for (WebElement ele1 : firstColLinks) {
			System.out.println(ele1.getText());
		}

		// ********Printing all the links present in the 2nd column of the footer******
		System.out.println("********Printing all the links present in the 2nd column of the footer******");
		List<WebElement> secondColLinks = columnDriver.findElements(By.xpath(
				"//div[@class='col-12 col-sm-4'] //span[contains(text(),'Useful Links')]/following-sibling::ul/li/a"));
		System.out.println("Total links present in the 2nd column of footer section are: " + secondColLinks.size());
		for (WebElement ele1 : secondColLinks) {
			System.out.println(ele1.getText());
		}
		
		// ********Printing all the links present in the 3rd column of the footer******
		System.out.println("********Printing all the links present in the 3rd column of the footer******");
		List<WebElement> thirdColLinks = columnDriver.findElements(By.xpath(
				"//div[@class='col-12 col-sm-4'] //span[contains(text(),'Company')]/following-sibling::ul/li/a"));
		System.out.println("Total links present in the 3rd column of footer section are: " + thirdColLinks.size());
		for (WebElement ele1 : thirdColLinks) {
			System.out.println(ele1.getText());
		}
		driver.close();
	}

}
