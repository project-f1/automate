package selenium_basics;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

public class StaticDropDown {
	private static WebDriver driver;

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		co.addArguments("--no-sandbox");
		co.addArguments("--disable-notification");
		driver = new ChromeDriver(co);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
		Select sel = new Select(driver.findElement(By.id("select-demo")));
		System.out.println("Size of the dropdown is: " + sel.getOptions().size());
		System.out.println("All option are as follows: ");
		List<WebElement> sel1 = sel.getOptions();
		for (WebElement opt : sel1) {
			System.out.println(opt.getText());

		}
		Thread.sleep(500);
		System.out.println("is this a multi select dropdown? - " + sel.isMultiple());
		sel.selectByValue("Thursday");
		// Thread.sleep(1500);
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src,
				new File("./src/test/resources/screenshot" + System.currentTimeMillis() + ".png" + "drpdwn"));
		driver.close();

	}

}
