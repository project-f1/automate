package selenium_basics;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BootstrapModals {
	private static WebDriver driver;
	private static Set<String> winHandles;
	private static Iterator<String> itr;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		co.addArguments("--no-sandbox");
		co.addArguments("--disable-notification");
		driver = new ChromeDriver(co);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.seleniumeasy.com/test/bootstrap-modal-demo.html");
		driver.findElement(By.xpath("//a[text()='Launch modal']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Save changes']")).click();
	}

}
