package selenium_basics;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWaiting {
	private static WebDriver driver;
	private static Set<String> winHandles;
	private static Iterator<String> itr;
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		ChromeOptions co = new ChromeOptions();
		co.addArguments("--start-maximized");
		co.addArguments("--no-sandbox");
		co.addArguments("--disable-notification");
		driver = new ChromeDriver(co);
		//JavascriptExecutor js = (JavascriptExecutor) driver;
		WebDriverWait wait = new WebDriverWait(driver, 10000);
		driver.get("https://www.seleniumeasy.com/test/dynamic-data-loading-demo.html");
		driver.findElement(By.id("save")).click();
		
		//WebElement ele = driver.findElement(By.partialLinkText("women/58.jpg"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("loading")));
		System.out.println(driver.findElement(By.partialLinkText(".jpg")).isDisplayed());
		driver.close();
	}

}
