package seleniumCodingChallenges;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GetPopulationCount {
	private static WebDriver driver;

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "./src/test/resources/driver_executables/geckodriver.exe");
		 driver = new FirefoxDriver();
		//driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.worldometers.info/world-population/");
		while (true) {
			List<WebElement> popCount = driver
					.findElements(By.xpath("//div[@class='maincounter-number']/span[@class='rts-counter']"));
			for (WebElement e : popCount) {
				System.out.println(e.getText());
			}
		}

	}
}
