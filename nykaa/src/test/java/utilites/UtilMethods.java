package utilites;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class UtilMethods {
	private static WebDriver driver;
	public WebDriver launchChrome(String url) {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/driver_executables/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		return driver;
	}
}
